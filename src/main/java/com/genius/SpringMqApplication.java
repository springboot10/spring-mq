package com.genius;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication()
@ComponentScan({"com.genius"})

public class SpringMqApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringMqApplication.class, args);
	}
}


