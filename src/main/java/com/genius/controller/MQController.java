package com.genius.controller;

import com.genius.service.MQService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
@RequestMapping(value = "/api/")
@RequiredArgsConstructor
public class MQController {
    final MQService mqService;

    @RequestMapping(value = "mq/", method = RequestMethod.POST)
    public String setDataToMQ(@RequestBody String data) {
        return mqService.setDataToMQService(data);
    }

    @RequestMapping(value = "mq/", method = RequestMethod.GET)
    public String getDataFromMQ() {
        return mqService.getDataFromMQService();
    }

}