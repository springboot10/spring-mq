package com.genius.service.impl;

import com.genius.service.MQService;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.JmsException;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@EnableJms
@Transactional
@RequiredArgsConstructor
@Log
public class MQServiceImpl implements MQService {
    final private JmsTemplate jmsTemplate;
    @Value("${app.dev.queue.name}")
    private String queueName;

    @Override
    public String setDataToMQService(String data) {
        String response = null;
        try {
            log.info("Sen data :" + data + " to queue :" + queueName);
            jmsTemplate.convertAndSend(queueName, data);
            response = "OK";
        } catch (JmsException ex) {
            ex.printStackTrace();
            response = "FAIL";
        }
        log.info("response :" + response);
        return response;
    }
    @Override
    public String getDataFromMQService() {
        String response = null;
        try {
            response = jmsTemplate.receiveAndConvert(queueName).toString();
            log.info("Get data :" + response + " from queue :" + queueName);
        } catch (JmsException ex) {
            ex.printStackTrace();
            response = "FAIL";
        }
        log.info("response :" + response);
        return response;
    }
}