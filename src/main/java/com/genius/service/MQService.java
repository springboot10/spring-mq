package com.genius.service;

public interface MQService {
    public String setDataToMQService(String data);

    public String getDataFromMQService();
}
